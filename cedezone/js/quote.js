 $(document).ready(function () {

     Quote.init();

     $("#category").hide();
     $('#datepicker').datepicker({
         dateFormat: 'dd/mm/yy',
     });


     $('#submit').click(function (e) {

         e.preventDefault;
         //   var selectedservice = $('#service_id').val();
         var selectedservice = $('#service_id option:selected').text();
         var selectedcategory = $('#category_id option:selected').text();
         //     alert(selectedservice);
         var location = $('#location_id').val();
         var service = $('#service_id').val();
         var category = $('#category_id').val();

         if (location == '') {
             alert('Select a Location');
             return false;
         } else if (service == '') {
             alert('Select a Service');
             return false;
         } else {

             if (selectedservice == 'Laundry') {
                 $("#laundry").modal("show");
                 return false;
             } else {
                 if (category == '') {
                     alert('Select a Service Category');
                     return false;
                 } else {
                     document.getElementById("cleaningtitle").innerHTML = selectedcategory + ' ' + selectedservice;
                     Quote.getQuote(location, service, category);
                 }
             }
         }
     });


     $('#service_id').on('change', function () {
         var selectedservice = $(this).val();
         // var selectedservice = $('#service_id option:selected').text();
         // var selectedservice = $('#service_id').options['#service_id'.selectedIndex].text
         // alert(selectedservice);
         if (selectedservice == 2) {
             $("#category").hide();
             //call get laundry price
         } else {
             $("#category").show();
         }
     });

     /*   $('#housetype').on('change', function () {
            var selectedhouse = $('#housetype option:selected').text();

            if (selectedhouse == 'Apartment/Flats') {
                $("#livingrooms").hide();
                $("#bedrooms").show();
                $("#bathrooms").show();
                $("#garden").hide();

            } else if (selectedhouse == 'Duplex' || selectedhouse == 'Mansion') {
                $("#livingrooms").show();
                $("#bedrooms").show();
                $("#bathrooms").show();
                $("#garden").show();
            } else {
                $("#livingrooms").hide();
                $("#bedrooms").hide();
                $("#bathrooms").hide();
                $("#garden").hide();
            }
        });*/

 });

 function getQuote() {
     var selectedservice = $('#service_id option:selected').text();

     if (selectedservice.contains('Laundry')) {

         $("#laundry").modal("show");

         return false;

     } else if (selectedservice.contains('Cleaning')) {
         document.getElementById("cleaningtitle").innerHTML = selectedservice;
         $("#cleaning").modal("show");

         return false;

     } else {
         alert('Select a Service');
         return false;
     }
 };
