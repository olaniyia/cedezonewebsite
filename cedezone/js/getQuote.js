Quote = {
    CONSTANTS: {
        route: 'http://localhost:8080/cleaning-app/public/api/v1/',
        country_route: 'country/all',
        state_route: 'country/states/',
        quote_route: 'fetch/price',
        service_route: 'service/all',
        category_route: 'category/all',
        location_route: 'location/'
    },
    init: function () {

        //   alert('iam here');
        Quote.getCountries();
        Quote.getService();
        Quote.getCategory();

        $('#country_id').change(function () {
            var value = $('#country_id').val();
            Quote.getStatesInCountry(value);
        });
        $('#state_id').change(function () {
            var value = $('#state_id').val();
            Quote.getLocationsInState(value);
        })
    },

    getQuote: function (location_id, service_id, category_id) {
        $.ajax({
            url: Quote.CONSTANTS.route + Quote.CONSTANTS.quote_route,
            data: {
                format: 'json',
                category_id: category_id,
                location_id: location_id,
                service_id: service_id
             //   serviceattribute_id: 1,
            },
            error: function () {
                $('#alert').modal();
                document.getElementById('alertresponse').innerHTML = 'Our Prices Does not cover this service Category yet'
            },
            dataType: 'json',
            success: function (data) {
                console.log(data);
                // alert(data.data)
                Quote.populateTable(data);

            },
            type: 'POST'
        });
    },

    populateTable: function (response) {

        if (response.status == true) {
            var $tr = '';
            var responses = response.data;
            $('#serviceQuoteTable tbody').html(""); ///empty table for new records
            var no = 0;
            $.each(responses, function (i, item) {
                no++
                $tr = $('<tr>').append(
                    $('<td>').text(no),
                    $('<td>').text(item.attribute),
                    $('<td>').text(item.recommended_hour),
                    $('<td>').text(item.price)
                );
                $('#serviceQuoteTable tbody').append($tr);
                $("#cleaning").modal();
            });
        } else {
            document.getElementById('alertresponse').innerHTML = response.msg;
            $('#alert').modal();
        }
    },
    getCountries: function () {
        $.ajax({
            url: Quote.CONSTANTS.route + Quote.CONSTANTS.country_route,
            data: {
                format: 'json'
            },
            error: function () {
                //   alert('Sorry error occured while fetching available countries');
                $('#alert').modal();
                document.getElementById('alertresponse').innerHTML = 'Error occured while fetching available countries'
            },
            dataType: 'json',
            success: function (data) {
                // console.log(data);
                Quote.populateCountryDropdown(data);
            },
            type: 'GET'
        });
    },
    populateCountryDropdown: function (data) {
        var $country = $("#country_id");
        $country.empty();
        $country.append('<option value="">Select your country</option>')
        $.each(data, function (index, value) {
            //console.log(data)
            $country.append("<option value=" + value.id + ">" + value.name + "</option>");
        });
    },

    getStatesInCountry: function (countryid) {
        $.ajax({
            url: Quote.CONSTANTS.route + Quote.CONSTANTS.state_route + countryid,
            data: {
                format: 'json'
            },
            error: function () {
                $('#alert').modal();
                document.getElementById('alertresponse').innerHTML = 'Error occured while fetching available states in your country'
            },
            dataType: 'json',
            success: function (data) {
                if (data.length == 0) {
                    //  alert('no state');
                    $('#empty_response').modal();
                    document.getElementById('noyet').innerHTML = "We are yet to Cover Your Country";
                } else {
                    Quote.populateStateDropdown(data);
                }
            },
            type: 'GET'
        });
    },

    populateStateDropdown: function (data) {
        var $state = $("#state_id");
        $state.empty();
        $state.append('<option value="">Select your state</option>');
        $.each(data, function (index, value) {
            $state.append("<option value=" + value.id + ">" + value.name + "</option>");
        });
    },

    getLocationsInState: function (stateid) {
        $.ajax({
            url: Quote.CONSTANTS.route + Quote.CONSTANTS.location_route + stateid,
            data: {
                format: 'json'

            },
            error: function () {
                alert('Sorry error occured while fetching available locations in your state');
            },
            dataType: 'json',
            success: function (data) {
                if (data.length == 0) {
                    //  alert('no state');
                    $('#empty_response').modal();
                    document.getElementById('noyet').innerHTML = "We are yet to Cover Your State";
                } else {
                    Quote.populateLocationDropdown(data);
                }
            },
            type: 'GET'
        });
    },

    populateLocationDropdown: function (data) {
        var $state = $("#location_id");
        $state.empty();
        $state.append('<option value="">Select your Location</option>');
        $.each(data, function (index, value) {
            $state.append("<option value=" + value.id + ">" + value.name + "</option>");
        });
    },

    getService: function () {
        $.ajax({
            url: Quote.CONSTANTS.route + Quote.CONSTANTS.service_route,
            data: {
                format: 'json'
            },
            error: function () {
                alert('Sorry error occured while fetching available services');
            },
            dataType: 'json',
            success: function (data) {
                console.log(data);
                Quote.populateServiceDropdown(data);

            },
            type: 'GET'
        });
    },

    populateServiceDropdown: function (data) {
        var $state = $("#service_id");
        $state.empty();
        $state.append('<option value="">Select Service</option>');
        $.each(data.data, function (index, value) {
            $state.append("<option value=" + value.id + ">" + value.name + "</option>");
        });
    },

    getCategory: function () {
        $.ajax({
            url: Quote.CONSTANTS.route + Quote.CONSTANTS.category_route,
            data: {
                format: 'json'
            },
            error: function () {
                alert('Sorry error occured while fetching available Service Categories');
            },
            dataType: 'json',
            success: function (data) {
                console.log(data);
                Quote.populatecategoryDropdown(data);

            },
            type: 'GET'
        });
    },

    populatecategoryDropdown: function (data) {
        var $state = $("#category_id");
        $state.empty();
        $state.append('<option value="">Select Category</option>');
        $.each(data.data, function (index, value) {
            $state.append("<option value=" + value.id + ">" + value.name + "</option>");
        });
    },

}
